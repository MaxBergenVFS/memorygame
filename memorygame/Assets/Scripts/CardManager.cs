﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardManager : MonoBehaviour
{
    [SerializeField] private Card[] _cards;
    [SerializeField] private Sprite[] _flippedCardSprites;

    private int _numOfUniqueCards;
    private List<int> _cardIDsAlreadyUsed = new List<int>();

    private void Start()
    {
        if (_cards.Length % 2 != 0) Debug.LogError("Odd number of cards in scene");
        _numOfUniqueCards = _cards.Length / 2;
        AssignSprites();
    }

    private void AssignSprites() 
    {
        foreach(Card card in _cards) 
        {
            int spriteNumToAssign = GetValidCardID();
            card.SetFlippedSprite(_flippedCardSprites[spriteNumToAssign]);
            card.cardID = spriteNumToAssign;
            _cardIDsAlreadyUsed.Add(spriteNumToAssign);
        }
    }

    private int GetValidCardID()
    {
        int ID = Random.Range(0, _numOfUniqueCards);
        return ID;
    }
}
