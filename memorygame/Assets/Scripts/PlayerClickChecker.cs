﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClickChecker : MonoBehaviour
{
    private Camera _cam;

    private void Awake()
    {
        _cam = Camera.main;
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = _cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);

            if (hit.collider == null) return;

            hit.transform.GetComponent<Card>()?.FlipCard();
        }
    }
}
