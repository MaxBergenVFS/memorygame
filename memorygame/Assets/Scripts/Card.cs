﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour
{
    [SerializeField] private GameObject _defaultCard;
    [SerializeField] private SpriteRenderer _spriteOnFlippedSide;

    private bool _isFlipped = false;
    
    public int cardID;
    
    public void FlipCard()
    {
        //print($"flipped {this.gameObject.name}");
        if(_isFlipped){ResetCard();}
        else
        {
            _defaultCard.SetActive(false);
            _isFlipped = true;
        }
    }

    private void ResetCard()
    {
        _isFlipped = false;
        _defaultCard.SetActive(true);
    }

    public void SetFlippedSprite(Sprite sprite)
    {
        _spriteOnFlippedSide.sprite = sprite;
    }
}
